<?php

/**
 * Weight textfield validation function
 */
function tiny_menu_editor_valid_weight($element, &$form_state) {
  if ((isset($element['#value']) && $element['#value'] !== '') || !empty($element['#required'])) {
    if (!preg_match('/^\-?\d+$/', $element['#value'])) {
       form_error($element, t('Value of weight is incorrect'));
    }
  }
}

/**
 * Tiny version of _menu_overview_tree_form()
 */
function _tiny_menu_editor_form($tree) {
  static $form = array('#tree' => TRUE);
  foreach ($tree as $data) {
    $title = '';
    $item = $data['link'];
    // Don't show callbacks; these have $item['hidden'] < 0.
    if ($item && $item['hidden'] >= 0) {
      $mlid = 'mlid:'. $item['mlid'];
      $form[$mlid]['#item'] = $item;
      $form[$mlid]['#attributes'] = $item['hidden'] ? array('class' => 'menu-disabled') : array('class' => 'menu-enabled');
      $form[$mlid]['title']['#value'] = 
        (variable_get('tiny_menu_editor_item_link', FALSE)
          ? check_plain($item['title'])
          : l($item['title'], $item['href'], $item['localized_options']))
        . ($item['hidden'] ? ' ('. t('disabled') .')' : '');
      //Can't completely remove ['hidden'] element because in theming function we check for its existence
      $form[$mlid]['hidden'] = variable_get('tiny_menu_editor_enabled', FALSE) ? array() : array(
        '#type' => 'checkbox',
        '#default_value' => !$item['hidden'],
      );
      if (!variable_get('tiny_menu_editor_expanded', FALSE)) {
        $form[$mlid]['expanded'] = array(
          '#type' => 'checkbox',
          '#default_value' => $item['expanded'],
        );
      }
      $form[$mlid]['weight'] = variable_get('tiny_menu_editor_weight', TRUE) ? array(
        '#type' => 'textfield',
        '#default_value' => isset($form_state[$mlid]['weight']) ? $form_state[$mlid]['weight'] : $item['weight'],
        '#size' => 4,
        '#element_validate' => array('tiny_menu_editor_valid_weight'),
      ) : array(
        '#type' => 'weight',
        '#delta' => 50,
        '#default_value' => isset($form_state[$mlid]['weight']) ? $form_state[$mlid]['weight'] : $item['weight'],
      );
      $form[$mlid]['mlid'] = array(
        '#type' => 'hidden',
        '#value' => $item['mlid'],
      );
      $form[$mlid]['plid'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($form_state[$mlid]['plid']) ? $form_state[$mlid]['plid'] : $item['plid'],
        '#size' => 6,
      );
      // Build a list of operations.
      $operations = array();
      $operations['edit'] = l(t('edit'), 'admin/build/menu/item/'. $item['mlid'] .'/edit');
      // Only items created by the menu module can be deleted.
      if ($item['module'] == 'menu' || $item['updated'] == 1) {
        $operations['delete'] = l(t('delete'), 'admin/build/menu/item/'. $item['mlid'] .'/delete');
      }
      // Set the reset column.
      elseif ($item['module'] == 'system' && $item['customized']) {
        $operations['reset'] = l(t('reset'), 'admin/build/menu/item/'. $item['mlid'] .'/reset');
      }

      $form[$mlid]['operations'] = array();
      foreach ($operations as $op => $value) {
        $form[$mlid]['operations'][$op] = array('#value' => $value);
      }
    }

    if ($data['below']) {
      _tiny_menu_editor_form($data['below']);
    }
  }
  return $form;
}

/**
 * Tiny version of menu_overview_form()
 */
function tiny_menu_editor_form(&$form_state, $menu) {
  global $menu_admin;
  $sql = "
    SELECT m.load_functions, m.to_arg_functions, m.access_callback, m.access_arguments, m.page_callback, m.page_arguments, m.title, m.title_callback, m.title_arguments, m.type, m.description, ml.*
    FROM {menu_links} ml LEFT JOIN {menu_router} m ON m.path = ml.router_path
    WHERE ml.menu_name = '%s'
    ORDER BY p1 ASC, p2 ASC, p3 ASC, p4 ASC, p5 ASC, p6 ASC, p7 ASC, p8 ASC, p9 ASC";
  $result = db_query($sql, $menu['menu_name']);
  $tree = menu_tree_data($result);
  $node_links = array();
  menu_tree_collect_node_links($tree, $node_links);
  // We indicate that a menu administrator is running the menu access check.
  $menu_admin = TRUE;
  menu_tree_check_access($tree, $node_links);
  $menu_admin = FALSE;

  $form = _tiny_menu_editor_form($tree);
  $form['#menu'] =  $menu;
  if (element_children($form)) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    );
  }
  else {
    $form['empty_menu'] = array('#value' => t('There are no menu items yet.'));
  }
  return $form;
}

/**
 * Tiny version of menu_overview_form_submit()
 */
function tiny_menu_editor_form_submit($form, &$form_state) {
  // When dealing with saving menu items, the order in which these items are
  // saved is critical. If a changed child item is saved before its parent,
  // the child item could be saved with an invalid path past its immediate
  // parent. To prevent this, save items in the form in the same order they
  // are sent by $_POST, ensuring parents are saved first, then their children.
  // See http://drupal.org/node/181126#comment-632270
  $order = array_flip(array_keys($form['#post'])); // Get the $_POST order.
  $form = array_merge($order, $form); // Update our original form with the new order.

  $updated_items = array();
  $fields = array('weight', 'plid');
  if (!variable_get('tiny_menu_editor_expanded', FALSE)) {
    $fields[] = 'expanded';
  }
  
  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['#item'])) {
      $element = $form[$mlid];
      // Update any fields that have changed in this menu item.
      foreach ($fields as $field) {
        if ($element[$field]['#value'] != $element[$field]['#default_value']) {
          $element['#item'][$field] = $element[$field]['#value'];
          $updated_items[$mlid] = $element['#item'];
        }
      }
      // Hidden is a special case, the value needs to be reversed.
      if (!variable_get('tiny_menu_editor_enabled', FALSE)) {
        if ($element['hidden']['#value'] != $element['hidden']['#default_value']) {
          $element['#item']['hidden'] = !$element['hidden']['#value'];
          $updated_items[$mlid] = $element['#item'];
        }
      }
    }
  }

  // Save all our changed items to the database.
  foreach ($updated_items as $item) {
    $item['customized'] = 1;
    menu_link_save($item);
  }
}

/**
 * Tiny version of theme_menu_overview_form()
 */
function theme_tiny_menu_editor_form($form) {
  drupal_add_tabledrag('menu-overview', 'match', 'parent', 'menu-plid', 'menu-plid', 'menu-mlid', TRUE, MENU_MAX_DEPTH - 1);
  drupal_add_tabledrag('menu-overview', 'order', 'sibling', 'menu-weight');

  $header = array(
    t('Menu item'),
  );
  if (!variable_get('tiny_menu_editor_enabled', FALSE)) {
    $header[] = array('data' => t('Enabled'), 'class' => 'checkbox');
  }
  if (!variable_get('tiny_menu_editor_expanded', FALSE)) {
    $header[] = array('data' => t('Expanded'), 'class' => 'checkbox');
  }
  $header[] = t('Weight');
  $header[] = array('data' => t('Operations'), 'colspan' => '3');

  $rows = array();
  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['hidden'])) {
      $element = &$form[$mlid];
      // Build a list of operations.
      $operations = array();
      foreach (element_children($element['operations']) as $op) {
        $operations[] = drupal_render($element['operations'][$op]);
      }
      while (count($operations) < 2) {
        $operations[] = '';
      }

      // Add special classes to be used for tabledrag.js.
      $element['plid']['#attributes']['class'] = 'menu-plid';
      $element['mlid']['#attributes']['class'] = 'menu-mlid';
      $element['weight']['#attributes']['class'] = 'menu-weight';

      // Change the parent field to a hidden. This allows any value but hides the field.
      $element['plid']['#type'] = 'hidden';

      $row = array();
      $row[] = theme('indentation', $element['#item']['depth'] - 1) . drupal_render($element['title']);
      if (!variable_get('tiny_menu_editor_enabled', FALSE)) {
        $row[] = array('data' => drupal_render($element['hidden']), 'class' => 'checkbox');
      }
      if (!variable_get('tiny_menu_editor_expanded', FALSE)) {
        $row[] = array('data' => drupal_render($element['expanded']), 'class' => 'checkbox');
      }
      $row[] = drupal_render($element['weight']) . drupal_render($element['plid']) . drupal_render($element['mlid']);
      $row = array_merge($row, $operations);

      $row = array_merge(array('data' => $row), $element['#attributes']);
      $row['class'] = !empty($row['class']) ? $row['class'] .' draggable' : 'draggable';
      $rows[] = $row;
    }
  }
  $output = '';
  if ($rows) {
    $output .= theme('table', $header, $rows, array('id' => 'menu-overview'));
  }
  $output .= drupal_render($form);
  return $output;
}

/**
 * Tiny Menu Editor settings form
 */
function tiny_menu_editor_settings($state) {
  $form = array(
    'tiny_menu_editor_weight' => array(
      '#type' => 'checkbox',
      '#title' => t('Replace "Weight" select with textfield'),
      '#description' => t('The memory savings: <b>HUGE</b><br />The usability loss: very low'),
      '#default_value' => variable_get('tiny_menu_editor_weight', TRUE),
    ),
    'tiny_menu_editor_item_link' => array(
      '#type' => 'checkbox',
      '#title' => t('Display menu item title as plain text'),
      '#description' => t('The memory savings: low<br />The usability loss: middle'),
      '#default_value' => variable_get('tiny_menu_editor_item_link', FALSE),
    ),
    'tiny_menu_editor_enabled' => array(
      '#type' => 'checkbox',
      '#title' => t('Remove "Enabled" options from tree'),
      '#description' => t('The memory savings: low<br />The usability loss: high'),
      '#default_value' => variable_get('tiny_menu_editor_enabled', FALSE),
    ),
    'tiny_menu_editor_expanded' => array(
      '#type' => 'checkbox',
      '#title' => t('Remove "Expanded" options from tree'),
      '#description' => t('The memory savings: low<br />The usability loss: high'),
      '#default_value' => variable_get('tiny_menu_editor_expanded', FALSE),
    ),
  );
  return system_settings_form($form);
}
